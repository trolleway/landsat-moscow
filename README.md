# landsat-moscow

generate before-after pictures from Landsat imagery

# install

```
git clone https://gitlab.com/trolleway/landsat-moscow.git
cd landsat-moscow.git

# create file usgs.txt with creds for http://earthexplorer.usgs.gov

cat > usgs.txt << EOF
login password
EOF

#enshure than you has QGIS ang GDAL installed

#This wil download python script for downloading Landsat from Earthexplorer, and alaska-scripts, with used for pansharpening.

./install.sh

#run

./process.sh

#This download landsat scenes from Earthexplorer, perform panshaprening and cropping. List of scenes and cropping coordinates stored in process.sh file

#Now, open TIFs in any version of QGIS, do color corrections, and save as RGB geotiff in folder "cropped_rgbs"

#convert geotiffs to JPEG ready to upload as images to websites
cd cropped_rgbs
./geotiffs2jpeg.sh
