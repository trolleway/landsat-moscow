#!/bin/bash

#convert geotiff to jpeg

for fullfile in *.tif
do

  filename="${fullfile##*/}"
  extension="${filename##*.}"
  filename="${filename%.*}"
  gdal_translate -of JPEG -outsize 0 720 -r cubic -co worldfile=no -co quality=95  ${fullfile} ${filename}.jpg
done
