#!/bin/bash

#use scenes ids from earthexplorer.usgs.gov

cat > scenes.txt << EOF
moscow_2000 LE71790212000273SGS00
moscow_2019 LC81790212019157LGN00
EOF

if [ ! -e scenes/moscow_2000 ]; then
    echo "Downloaded scenes not found!"
    mkdir scenes
    python LANDSAT-Download/download_landsat_scene.py -o liste -l $(pwd)/scenes.txt -u $(pwd)/usgs.txt --output scenes
fi


#3-2-1
tar -xvzf scenes/moscow_2000/LE71790212000273SGS00.tgz
SCENE=LE07_L1TP_179021_20000929_20170209_01_T1
gdal_landsat_pansharp -rgb ${SCENE}_B3.TIF -rgb ${SCENE}_B2.TIF -rgb ${SCENE}_B1.TIF \
  -lum ${SCENE}_B2.TIF 0.25 -lum ${SCENE}_B3.TIF 0.23 -lum ${SCENE}_B4.TIF 0.52 \
  -pan ${SCENE}_B8.TIF -ndv 0 -o 20000929_pan.TIF

rm ${SCENE}_*

#4-3-2
rm 20190606_pan.TIF

tar -xvzf scenes/moscow_2019/LC81790212019157LGN00.tgz
LANDSAT_PRODUCT_IDENTIFER=LC08_L1TP_179021_20190606_20190619_01_T1
gdal_landsat_pansharp -rgb ${LANDSAT_PRODUCT_IDENTIFER}_B4.TIF -rgb ${LANDSAT_PRODUCT_IDENTIFER}_B3.TIF -rgb ${LANDSAT_PRODUCT_IDENTIFER}_B2.TIF \
  -lum ${LANDSAT_PRODUCT_IDENTIFER}_B2.TIF 0.25 -lum ${LANDSAT_PRODUCT_IDENTIFER}_B3.TIF 0.5 -lum ${LANDSAT_PRODUCT_IDENTIFER}_B4.TIF 0.5 \
  -pan ${LANDSAT_PRODUCT_IDENTIFER}_B8.TIF -ndv 0 -o 20190606_pan.TIF

rm ${LANDSAT_PRODUCT_IDENTIFER}_*

#crop using -te tag to simpler and more rectangle borders
#too hard draw rectangle in UTM in QGIS and save it to file

borders="388170 6159911 393946 6166109"
name=Vnukovo
scene=20000929_pan
gdalwarp -dstalpha -overwrite -co COMPRESS=LZW -te ${borders} -crop_to_cutline  ${scene}.TIF ${scene}-${name}.TIF
scene=20190606_pan
gdalwarp -dstalpha -overwrite -co COMPRESS=LZW -te ${borders} -crop_to_cutline  ${scene}.TIF ${scene}-${name}.TIF

borders="395158 6201335 404813 6206957"
name=Sheremetyevo
scene=20000929_pan
gdalwarp -dstalpha -overwrite -co COMPRESS=LZW -te ${borders} -crop_to_cutline  ${scene}.TIF ${scene}-${name}.TIF
scene=20190606_pan
gdalwarp -dstalpha -overwrite -co COMPRESS=LZW -te ${borders} -crop_to_cutline  ${scene}.TIF ${scene}-${name}.TIF

name=Sovkhoz_Moskovskiy
borders="394928 6159414 399822 6165212"
scene=20000929_pan
gdalwarp -dstalpha -overwrite -co COMPRESS=LZW -te ${borders} -crop_to_cutline  ${scene}.TIF ${name}-${scene}-Landsat7.tif
scene=20190606_pan
gdalwarp -dstalpha -overwrite -co COMPRESS=LZW -te ${borders} -crop_to_cutline  ${scene}.TIF ${name}-${scene}-Landsat8.tif

name=Sovkhoz_Kommunarka
borders="402525 6156863 405968 6161163"
scene=20000929_pan
gdalwarp -dstalpha -overwrite -co COMPRESS=LZW -te ${borders} -crop_to_cutline  ${scene}.TIF ${name}-${scene}-Landsat7.tif
scene=20190606_pan
gdalwarp -dstalpha -overwrite -co COMPRESS=LZW -te ${borders} -crop_to_cutline  ${scene}.TIF ${name}-${scene}-Landsat8.tif

name=Luberetskiye_Polya
borders="429332 6172356 436222 6176185"
scene=20000929_pan
gdalwarp -dstalpha -overwrite -co COMPRESS=LZW -te ${borders} -crop_to_cutline  ${scene}.TIF ${name}-${scene}-Landsat7.tif
scene=20190606_pan
gdalwarp -dstalpha -overwrite -co COMPRESS=LZW -te ${borders} -crop_to_cutline  ${scene}.TIF ${name}-${scene}-Landsat8.tif

name=Химкинский_лес
borders="400117 6196451 405725 6202746"
scene=20000929_pan
gdalwarp -dstalpha -overwrite -co COMPRESS=LZW -te ${borders} -crop_to_cutline  ${scene}.TIF ${name}-${scene}-Landsat7.tif
scene=20190606_pan
gdalwarp -dstalpha -overwrite -co COMPRESS=LZW -te ${borders} -crop_to_cutline  ${scene}.TIF ${name}-${scene}-Landsat8.tif

name=Krasnogorsk
borders="392467 6184949 399324 6189835"
scene=20000929_pan
gdalwarp -dstalpha -overwrite -co COMPRESS=LZW -te ${borders} -crop_to_cutline  ${scene}.TIF ${name}-${scene}-Landsat7.tif
scene=20190606_pan
gdalwarp -dstalpha -overwrite -co COMPRESS=LZW -te ${borders} -crop_to_cutline  ${scene}.TIF ${name}-${scene}-Landsat8.tif

name=Park_Patriot
borders="357680 6156636 365316 6162078"
scene=20000929_pan
gdalwarp -dstalpha -overwrite -co COMPRESS=LZW -te ${borders} -crop_to_cutline  ${scene}.TIF ${name}-${scene}-Landsat7.tif
scene=20190606_pan
gdalwarp -dstalpha -overwrite -co COMPRESS=LZW -te ${borders} -crop_to_cutline  ${scene}.TIF ${name}-${scene}-Landsat8.tif