#!/bin/bash

name=Aeroport_ZIA
borders="443091 6154077 451139 6159712"
scene="S2A_MSIL1C_20190606T083601_N0207_R064_T37UDB_20190606T104250"
gdalwarp -dstalpha -overwrite -co COMPRESS=LZW -te ${borders} -crop_to_cutline  ${scene}.tif ${name}-${scene}-Sentinel2B.tif
gdal_translate -of JPEG -outsize 0 720 -r cubic -co worldfile=no -co quality=95  ${name}-${scene}-Sentinel2B.tif ${name}-${scene}-Sentinel2B.jpg

name=Aeroport_ZIA_Corona
borders="443091 6154077 451139 6159712"
scene="zia-zrop-unref_modified"
gdalwarp -dstalpha -overwrite  -te ${borders} -crop_to_cutline  ${scene}.tif ${name}-${scene}-Corona.tif
gdal_translate -of JPEG -expand RGBA -co worldfile=no -co quality=95  ${name}-${scene}-Corona.tif ${name}-${scene}-Corona.jpg